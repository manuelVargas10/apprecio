import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/material.dart';

class Scanner extends StatefulWidget {
  const Scanner({Key? key}) : super(key: key);

  @override
  _ScannerState createState() => _ScannerState();
}

class _ScannerState extends State<Scanner> {

  Barcode? result;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;
  @override
  Widget build(BuildContext context) { var scanArea = (MediaQuery.of(context).size.width < 400 ||
      MediaQuery.of(context).size.height < 400)
      ? 150.0
      : 300.0;
  return Scaffold(
    body: QRView(
      key: qrKey,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.red,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      cameraFacing: CameraFacing.back ,
      onQRViewCreated: _onQRViewCreated,
    ),
  );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      Navigator.pushReplacementNamed(context, "/Home", arguments: scanData.code);
    });
  }
}
