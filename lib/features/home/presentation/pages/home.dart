import 'package:apprecio/core/util/company_colors.dart';
import 'package:apprecio/core/util/company_fonts.dart';
import 'package:apprecio/core/widget/button/custom_button.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {

  final String codeScanner;

  const Home({Key? key, this.codeScanner = ""}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  static late Size size;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          widget.codeScanner.isNotEmpty ?  Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
              border: Border.all(
                color: CompanyColor.color().second
              )
            ),
            child: Text(widget.codeScanner, style: CompanyFontStyle.style().titleAppLight,),
          ) : Container(),
          Container(
            height: size.height * 0.085,
            margin: EdgeInsets.symmetric(
              horizontal: size.width * 0.12,
              vertical: size.height * 0.08,
            ),
            child: CustomButton(
              model: CustomButtonModel(
                label: "Escanear",
                handledButton: () => handledScanner(),
                color: CustomButtonColor.dark,
              ),
            ),
          )
        ],
      ),
    );
  }

  handledScanner() {
    Navigator.pushNamed(context, "/Scanner");
  }
}
