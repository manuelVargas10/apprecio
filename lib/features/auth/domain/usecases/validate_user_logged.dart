import 'package:apprecio/core/errors/failure.dart';
import 'package:apprecio/core/usecases/use_cases.dart';
import 'package:apprecio/features/auth/data/models/credentials_model.dart';
import 'package:apprecio/features/auth/domain/repositories/auth_repository.dart';
import 'package:dartz/dartz.dart';

class ValidateUserLogged extends UseCase<CredentialsModel, NoParams> {
  final AuthRepository authRepository;

  ValidateUserLogged({required this.authRepository});

  @override
  Future<Either<Failure, CredentialsModel>> call(NoParams params) {
    return authRepository.validateUserLogged();
  }
}
