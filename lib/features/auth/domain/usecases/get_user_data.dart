import 'package:apprecio/core/model/user_model.dart';
import 'package:apprecio/core/usecases/use_cases.dart';
import 'package:apprecio/features/auth/domain/repositories/auth_repository.dart';

import '../../../../core/errors/failure.dart';
import 'package:dartz/dartz.dart';

class GetUserData extends UseCase<UserModel, String> {
  final AuthRepository authRepository;

  GetUserData({required this.authRepository});

  @override
  Future<Either<Failure, UserModel>> call(String params) {
    return authRepository.getUserDb(params);
  }
}
