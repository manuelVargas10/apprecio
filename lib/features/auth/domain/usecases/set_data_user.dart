
import 'package:apprecio/core/errors/failure.dart';
import 'package:apprecio/core/model/user_model.dart';
import 'package:apprecio/core/usecases/use_cases.dart';
import 'package:apprecio/features/auth/domain/repositories/auth_repository.dart';
import 'package:dartz/dartz.dart';

class SetDataUSer extends UseCase<bool, UserModel> {
  final AuthRepository authRepository;

  SetDataUSer({required this.authRepository});

  @override
  Future<Either<Failure, bool>> call(UserModel userModel) {
    return authRepository.setDataUSer(userModel);
  }
}
