import 'package:dartz/dartz.dart';
import 'package:apprecio/core/errors/failure.dart';
import 'package:apprecio/core/usecases/use_cases.dart';
import 'package:apprecio/features/auth/data/models/credentials_model.dart';
import 'package:apprecio/features/auth/domain/repositories/auth_repository.dart';

class SaveUserLogged extends UseCase<bool, CredentialsModel> {
  final AuthRepository authRepository;

  SaveUserLogged({required this.authRepository});

  @override
  // ignore: avoid_renaming_method_parameters
  Future<Either<Failure, bool>> call(CredentialsModel credentialsModel) {
    return authRepository.saveUserLogged(credentialsModel);
  }
}
