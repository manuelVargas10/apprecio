import 'package:apprecio/features/auth/presentation/pages/auth.dart';
import 'package:apprecio/core/model/user_model.dart';
import 'package:apprecio/features/home/presentation/pages/home.dart';
import 'package:apprecio/features/new_user/presentation/pages/new_user.dart';
import 'package:apprecio/features/scaner/presentation/pages/scaner.dart';
import 'package:apprecio/splash_screen.dart';
import 'package:flutter/material.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {

    UserModel userModel= UserModel.fromJsonNoData();
    String code = "";
    final argument  = settings.arguments;

    if(argument is UserModel){
      userModel = argument;
    }if(argument is String){
      code = argument;
    }

    switch (settings.name) {
      case '/Scanner': return MaterialPageRoute(builder: (_) => const Scanner());
      case '/': return MaterialPageRoute(builder: (_) => const AppSplashScreen());
      case '/authPage': return MaterialPageRoute(builder: (_) => const AuthPage());
      case '/RegisterPage': return MaterialPageRoute(builder: (_) => const NewUser());
      case '/Home': return MaterialPageRoute(builder: (_) => Home(codeScanner: code));
      default: return MaterialPageRoute(builder: (_) => const AuthPage());
    }
  }}



