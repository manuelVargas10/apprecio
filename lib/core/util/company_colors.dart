import 'package:flutter/material.dart';

class CompanyColor {
  static const _defaultColor = Color(0x00000000);

  final Color primary;
  final Color primary40;
  final Color primary70;
  final Color second;
  final Color second40;
  final Color second70;
  final Color third;

 const CompanyColor({
    this.primary = _defaultColor,
    this.primary40 = _defaultColor,
    this.primary70 = _defaultColor,

    this.second = _defaultColor,
    this.second40 = _defaultColor,
    this.second70 = _defaultColor,

   this.third = _defaultColor,
  });

  factory CompanyColor.color(){
    return const CompanyColor(
      primary: Color(0xFFFFFFFF),
      primary40: Color(0xFFa1a1a1),
      primary70: Color(0xFF585858),
      second: Color(0xFFff4860),
      second40: Color.fromARGB(40, 255, 72, 96),
      second70: Color.fromARGB(70, 255, 72, 96),
      third: Colors.black,
    );
  }
}