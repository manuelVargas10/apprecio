class UserModel {
  UserModel({
    this.uid,
    this.token = "",
    this.image,
    this.dateUpdate,
    this.description,
    required this.rol,
    this.state = false,
    required this.name,
    required this.email,
    required this.dateCreate,
  });

  String rol;
  bool? state;
  String name;
  String? uid;
  String? token;
  String email;
  String? image;
  String? description;
  DateTime dateCreate;
  DateTime? dateUpdate;

  factory UserModel.fromJson(json, String id) => UserModel(
    uid: id,
    rol: json["rol"],
    name: json["name"],
    email: json["email"],
    token: json["token"],
    image: json["image"],
    // ignore: prefer_null_aware_operators
    dateCreate: json["dateCreate"] != null ? json["dateCreate"].toDate() : null,
    // ignore: prefer_null_aware_operators
    dateUpdate: json["dateUpdate"] != null ? json["dateUpdate"].toDate() : null,
    description: json["description"],
  );

  factory UserModel.fromJsonNoData() => UserModel(
    uid: "",
    rol: "",
    name: "",
    email: "",
    token: "",
    image: "",
    // ignore: prefer_null_aware_operators
    dateCreate: DateTime.now(),
    // ignore: prefer_null_aware_operators
    dateUpdate: DateTime.now(),
    description: "",
  );

  Map<String, dynamic> toJson() => {
    "rol": rol,
    "name": name,
    "token": token,
    "email": email,
    "image": image,
    "dateUpdate": dateUpdate,
    "dateCreate": dateCreate,
    "description": description,
  };

  Map<String, dynamic> toJsonGroup() => {
    "rol": rol,
    "uid": uid,
    "name": name,
    "email": email,
    "token": token,
    "image": image,
    "dateUpdate": dateUpdate,
    "dateCreate": dateCreate,
    "description": description,
  };
}
