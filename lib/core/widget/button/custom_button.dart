import 'package:apprecio/core/util/company_colors.dart';
import 'package:apprecio/core/util/company_fonts.dart';
import 'package:flutter/material.dart';

enum CustomButtonColor {
  light,
  dark,
}

class CustomButtonModel {
  final CustomButtonColor color;
  final String label;
  final Function handledButton;

  CustomButtonModel({
    this.label = "",
    required this.handledButton,
    this.color = CustomButtonColor.light,
  });
}

class CustomButton extends StatelessWidget {
  final CustomButtonModel model;

  const CustomButton({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => model.handledButton(),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: _getColor(model.color),
          border: Border.all(
            color: _getColorBorder(model.color),
            width: 2
          ),
          borderRadius: BorderRadius.circular(40),
          boxShadow: [
            BoxShadow(
                color: CompanyColor.color().second70,
                spreadRadius: 3,
                blurRadius: 3,
                offset: const Offset(0, 2))
          ],
        ),
        child: Text(
          model.label,
          style: _getColorText(model.color),
        ),
      ),
    );
  }

  Color _getColor(CustomButtonColor color) {
    switch (color) {
      case CustomButtonColor.light:
        return CompanyColor.color().primary;
      case CustomButtonColor.dark:
        return CompanyColor.color().second;
      default:
        return Colors.black;
    }
  }
  TextStyle _getColorText(CustomButtonColor color) {
    switch (color) {
      case CustomButtonColor.light:
        return CompanyFontStyle.style().buttonStyleSecond;
      case CustomButtonColor.dark:
        return CompanyFontStyle.style().buttonStyle;
      default:
        return CompanyFontStyle.style().buttonStyle;
    }
  }

  Color _getColorBorder(CustomButtonColor color) {
    switch (color) {
      case CustomButtonColor.light:
        return CompanyColor.color().second;
      case CustomButtonColor.dark:
        return CompanyColor.color().primary;
      default:
        return Colors.black;
    }
  }
}
