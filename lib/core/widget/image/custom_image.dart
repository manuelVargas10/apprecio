import 'package:flutter/cupertino.dart';
enum CustomImageIcon {
  icon1,
  icon2,
}


class CustomImageModel {
  final CustomImageIcon color;

  CustomImageModel({
    this.color = CustomImageIcon.icon1,
  });
}

class CustomImage extends StatelessWidget {
  final CustomImageModel model;

  const CustomImage({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.asset(_getColor(model.color));
  }

  String _getColor(CustomImageIcon color) {
    switch (color) {
      case CustomImageIcon.icon1:
        return "assets/image/Le0wl5kb_400x400.jpeg";
      case CustomImageIcon.icon2:
        return "assets/image/img2.png";
    }
  }
}
