import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:apprecio/core/util/constant.dart';
import 'package:apprecio/core/model/user_model.dart';
import 'package:apprecio/core/widget/image/custom_image.dart';
import 'package:apprecio/features/auth/presentation/bloc/auth_bloc.dart';

class AppSplashScreen extends StatefulWidget {
  const AppSplashScreen({Key? key}) : super(key: key);

  @override
  _AppSplashScreenState createState() => _AppSplashScreenState();
}

class _AppSplashScreenState extends State<AppSplashScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<AuthBloc>(context).add(ValidateUserLoggedEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (BuildContext context, state) {
        if (state is NoExistCredentialErrorState) {
          Navigator.pushReplacementNamed(context, "/authPage");
        }
        if (state is ExistCredentialState) {
          BlocProvider.of<AuthBloc>(context).add(
            LoginEvent(
              email: state.dataCredential.email,
              password: state.dataCredential.password,
            ),
          );
        }
        if (state is GetUserDataLoadedState) {
          handledGoHome(state.userModel);
        }
      },
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: CustomImage(
              model: CustomImageModel(
                color: CustomImageIcon.icon2
              ),
            ),
          ),
        ),
      ),
    );
  }

  handledGoHome(UserModel userModel) {
    if(userModel.rol == adminConstant){
      Navigator.pushReplacementNamed(context, "/adminPanel", arguments: userModel);
    }else{
    Navigator.pushReplacementNamed(context, "/Home", arguments: userModel);
    }
  }
}
